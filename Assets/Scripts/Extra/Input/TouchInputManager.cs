using System;
using UnityEngine;

[DefaultExecutionOrder(-10)]
public class TouchInputManager : Singleton<TouchInputManager>
{
    public delegate void TouchBeginDelegate(Touch touches);

    public event TouchBeginDelegate TouchBeginEvent;

    public delegate void TouchProcessDelegate(Touch touches);

    public event TouchProcessDelegate TouchProcessEvent;

    public delegate void TouchEndedDelegate(Touch touches);

    public event TouchEndedDelegate TouchEndedEvent;



    public delegate void TwoFingersBeginDelegate(Touch[] touches);

    public event TwoFingersBeginDelegate TwoFingersBeginEvent;

    public delegate void TwoFingersTouchProcessDelegate(Touch[] touches);

    public event TwoFingersTouchProcessDelegate TwoFingersTouchProcessEvent;

    public delegate void TwoFingersEndedDelegate(Touch[] touches);

    public event TwoFingersEndedDelegate TwoFingersEndedEvent;



    public delegate void BackButtonDelegate();

    public event BackButtonDelegate BackButtonEvent;

    [SerializeField] private bool simulateTouch;
    private Touch[] _simulatedTouches;
    private bool _twoFingersSimulated;
    private int _mouseButtonUpCounter;


    private void Awake()
    {
        if (simulateTouch) _simulatedTouches = new Touch[2];
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Return)) BackButtonEvent?.Invoke();

        if (simulateTouch)
        {
            var v = Input.mousePosition;
#if UNITY_EDITOR
            if (v.x < 0 || v.y < 0 || v.x > Screen.width || v.y > Screen.height) return;
#endif
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                if (!_twoFingersSimulated && Input.GetMouseButtonDown(0) && !PointerExt.IsPointerOverUIElement(v))
                {
                    _simulatedTouches[0].position = v;
                    _twoFingersSimulated = true;
                }
                else if (_twoFingersSimulated)
                {
                    if (Input.GetMouseButtonUp(0)) _mouseButtonUpCounter++;

                    if (Input.GetMouseButtonDown(0) && !PointerExt.IsPointerOverUIElement(v))
                    {
                        _simulatedTouches[1].position = v;
                        TwoFingersBeginEvent?.Invoke(_simulatedTouches);
                    }
                    else if (Input.GetMouseButton(0) && !PointerExt.IsPointerOverUIElement(v))
                    {
                        _simulatedTouches[1].position = v;
                        TwoFingersTouchProcessEvent?.Invoke(_simulatedTouches);
                    }
                    else if (_mouseButtonUpCounter > 1 && Input.GetMouseButtonUp(0))
                    {
                        TwoFingersEndedEvent?.Invoke(_simulatedTouches);
                    }
                }
            }
            else if (Input.GetKeyUp(KeyCode.LeftAlt))
            {
                _twoFingersSimulated = false;
                TwoFingersEndedEvent?.Invoke(_simulatedTouches);
                _mouseButtonUpCounter = 0;
            }
            else
            {
                if (Input.GetMouseButtonDown(0) && !PointerExt.IsPointerOverUIElement(v)) TouchBeginEvent?.Invoke(new Touch() {position = v});
                else if (Input.GetMouseButton(0) && !PointerExt.IsPointerOverUIElement(v)) TouchProcessEvent?.Invoke(new Touch() {position = v});
                if (Input.GetMouseButtonUp(0)) TouchEndedEvent?.Invoke(new Touch() {position = v});
            }
        }

        if (Input.touchCount == 1)
        {
            var touch = Input.touches[0];
            if (PointerExt.IsPointerOverUIElement(touch.position)) return;
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    TouchBeginEvent?.Invoke(touch);
                    break;
                case TouchPhase.Moved:
                case TouchPhase.Stationary:
                    TouchProcessEvent?.Invoke(touch);
                    break;
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    TouchEndedEvent?.Invoke(touch);
                    break;
            }
        }
        else if (Input.touchCount >= 2)
        {
            int activeTouches = 0;
            int beginTouches = 0;
            foreach (var touch in Input.touches)
            {
                if (!(touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended || PointerExt.IsPointerOverUIElement(touch.position))) activeTouches++;
                if (touch.phase == TouchPhase.Began) beginTouches++;
            }

            if (activeTouches < 2)
            {
                TwoFingersEndedEvent?.Invoke(Input.touches);
                return;
            }

            if (activeTouches - beginTouches < 2)
            {
                TwoFingersBeginEvent?.Invoke(Input.touches);
                return;
            }

            TwoFingersTouchProcessEvent?.Invoke(Input.touches);
        }
    }
}
