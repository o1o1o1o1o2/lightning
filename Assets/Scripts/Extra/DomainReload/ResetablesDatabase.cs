using System.Collections.Generic;
using UnityEngine;

public static class ResetablesDatabase
{
    private static List<IResetable> _resetables = new List<IResetable>();
 
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    static void ReInitStatic()
    {
        int i = _resetables.Count - 1;
        while (i >= 0)
        {
            IResetable resetable = _resetables[i];
            Object obj = resetable as Object;
       
            // null check, because == is overriden in the Object
            if (obj ?? false)
                resetable.ResetInternalState();
            else
                _resetables.RemoveAt(i);
 
            i--;
        }
    }
 
    public static void Register(IResetable resetable)
    {
        if (_resetables.Contains(resetable))
            return;
     
        _resetables.Add(resetable);
    }
 
    public static void UnRegister(IResetable resetable)
    {
        _resetables.Remove(resetable);
    }
}