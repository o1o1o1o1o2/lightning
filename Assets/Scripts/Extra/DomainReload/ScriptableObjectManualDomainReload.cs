using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectManualDomainReload : ScriptableObject
#if UNITY_EDITOR
    ,IResetable
#endif
{
    protected virtual void OnEnable()
    {
#if UNITY_EDITOR
        ResetablesDatabase.Register(this);        
#endif
    }

    protected virtual void OnDisable()
    {
#if UNITY_EDITOR
        ResetablesDatabase.UnRegister(this);
#endif
    }

    protected virtual void OnDestroy()
    {
#if UNITY_EDITOR
        ResetablesDatabase.UnRegister(this);
#endif
    }

#if UNITY_EDITOR
    public void ResetInternalState()
    {
        OnEnable();
    }
#endif
}
