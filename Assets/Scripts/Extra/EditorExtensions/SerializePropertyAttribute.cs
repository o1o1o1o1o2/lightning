using UnityEditor;
using UnityEngine;


public sealed class SerializePropertyAttribute : PropertyAttribute {
    public readonly string name;
    public bool dirty;
 
    public SerializePropertyAttribute(string name) {
        this.name = name;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(SerializePropertyAttribute))]
sealed class SerializeProperty : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializePropertyAttribute serializePropertyAttribute = (SerializePropertyAttribute) base.attribute;

        EditorGUI.BeginChangeCheck();
         
        EditorGUI.PropertyField(position, property, label);

        if (EditorGUI.EndChangeCheck())
        {
            serializePropertyAttribute.dirty = true;
        }
        else if (serializePropertyAttribute.dirty)
        {
            var parent = property.GetParentObject(property.propertyPath);

            var type = parent.GetType();
            var info = type.GetProperty(serializePropertyAttribute.name);

            if (info == null)
                Debug.LogError("Invalid property name \"" + serializePropertyAttribute.name + "\"");
            else
                info.SetValue(parent, fieldInfo.GetValue(parent), null);

            serializePropertyAttribute.dirty = false;
        }
    }
    
    public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
        
        float totalHeight = EditorGUI.GetPropertyHeight (property, label, true) + EditorGUIUtility.standardVerticalSpacing;
        
        return totalHeight;
    }
}

#endif