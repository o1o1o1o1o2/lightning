using System.Collections.Generic;
using UnityEngine;

public class SimplePool : Singleton<SimplePool>
{
   private Dictionary<string, Queue<IPoolable>> poolDictionary;

   private void Awake()
   {
      poolDictionary = new Dictionary<string, Queue<IPoolable>>();
   }

   public IPoolable GetFromPool(IPoolable poolItem)
   {
      IPoolable obj;

      if (!poolDictionary.ContainsKey(poolItem.poolTag))
      {
         poolDictionary.Add(poolItem.poolTag, new Queue<IPoolable>());
         var parent = new GameObject(poolItem.poolTag + "Pool");
         parent.transform.parent = transform;
      }

      if (poolDictionary[poolItem.poolTag].Count != 0)
      {
         obj = poolDictionary[poolItem.poolTag].Dequeue();
         obj.prefab.SetActive(true);
         return obj;
      }
      
      obj = Instantiate(poolItem.prefab, GameObject.Find(poolItem.poolTag + "Pool").transform).GetComponent<IPoolable>();
      return obj;
   }

   public void ReturnToPool(IPoolable poolItem)
   {
      poolItem.prefab.SetActive(false);
      poolDictionary[poolItem.poolTag].Enqueue(poolItem);
   }
}
