using UnityEngine;

[CreateAssetMenu(fileName = "IntVariable", menuName = "SO/IntVariable", order = 1)]
public class IntVariable : ScriptableVariable<int>
{
}

