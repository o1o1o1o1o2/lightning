using UnityEngine;

[CreateAssetMenu(fileName = "BoolVariable", menuName = "SO/BoolVariable", order = 1)]
public class BoolVariable : ScriptableVariable<bool>
{
}