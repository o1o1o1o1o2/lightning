using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Ext
{
    public static float Angle360(this Vector3 vec1, Vector3 vec2)
    {
        var v = Vector3.Angle(vec1, vec2);
        return  (vec2.y < vec1.y) ? 360.0f - v : v;
    }
}
