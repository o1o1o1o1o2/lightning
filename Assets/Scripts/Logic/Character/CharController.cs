using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

[Serializable]
public abstract class CharController : MonoBehaviour, IDamageble, IPoolable, IScalable
{
    private NavMeshAgent _navMeshAgent;
    
    [SerializeField]
    protected IntReference scores;

    [SerializeField] protected string _poolTag;
    public string poolTag => _poolTag;
    public GameObject prefab => gameObject;

    [SerializeField] protected int _currentHealth;
    public int currentHealth => _currentHealth;
    
    [SerializeField] protected int _minHealth;
    public int minHealth => _minHealth;

    [SerializeField] protected int _maxHealth;
    public int maxHealth => _maxHealth;

    [SerializeField] protected float _minScale;

    public float minScale => _minScale;

    [SerializeField] protected float _maxScale;

    public float maxScale => _maxScale;

    private void SetMovementSpeed()
    {
        _navMeshAgent.speed = Mathf.Max(3,20f / _currentHealth);
    }

    private Coroutine SetScaleRoutine = null;
    public virtual void SetScale()
    {
        if (SetScaleRoutine != null) StopCoroutine(SetScaleRoutine);
        var scale = minScale + (_maxScale - minScale) / maxHealth * currentHealth;
        SetScaleRoutine = StartCoroutine(SetScaleC(new Vector3(scale,1,scale)));
    }

    IEnumerator SetScaleC(Vector3 newScale)
    {
        var oldScale = transform.localScale;
        var t = 0.0f;
        while (true)
        {
            t += Time.deltaTime / GameSettings.Instance.attackSpeed;
            transform.localScale = Vector3.Lerp(oldScale, newScale, t);
            if (t >= 1f) yield break;
            yield return null;
        }
    }

    public void SetupCharacter(int minHealth, int maxHealth, float minScale, float maxScale, string tag)
    {
        _minHealth = minHealth;
        _maxHealth = maxHealth;
        _minScale = minScale;
        _maxScale = maxScale;
        _poolTag = tag;
    }

    public virtual void OnSpawn()
    {
        if (!_navMeshAgent)
        {
            _navMeshAgent = gameObject.AddComponent<NavMeshAgent>();
            _navMeshAgent.baseOffset = 0.5f;
            _navMeshAgent.stoppingDistance = maxScale;
            _navMeshAgent.Warp(transform.position);
        }
        else
        {
            _navMeshAgent.Warp(transform.position);
            _navMeshAgent.enabled = true;
        }

        _navMeshAgent.SetDestination(RandomEXT.GetRandomPositionAtTheEdgeOfScreen(new Vector2(GameSettings.Instance.gridSize.x, GameSettings.Instance.gridSize.y)));
        
        _currentHealth = Random.Range(minHealth, maxHealth);
        
        SetScale();
        SetMovementSpeed();
    }

    public void Update()
    {
        if (_navMeshAgent.enabled && !_navMeshAgent.pathPending && Vector3.Distance(transform.position, _navMeshAgent.pathEndPosition) < _navMeshAgent.stoppingDistance)
        {
            _navMeshAgent.isStopped = true;
            _navMeshAgent.enabled = false;
            SimplePool.Instance.ReturnToPool(this);
        }
    }

    public virtual void TakeDamage(int damageAmount)
    {
        DamagePopup.Create(transform.position,damageAmount, false);
        
        _currentHealth -= damageAmount;
        if (_currentHealth <= 0)
        {
            Deactivate();
            ModifyScores();
            return;
        }
        
        SetScale();
        SetMovementSpeed();
    }

    public virtual void Deactivate()
    {
        _navMeshAgent.isStopped = true;
        _navMeshAgent.enabled = false;
        SimplePool.Instance.ReturnToPool(this);
    }

    protected abstract void ModifyScores();

}
