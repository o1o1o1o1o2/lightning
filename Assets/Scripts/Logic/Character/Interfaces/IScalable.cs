public interface IScalable 
{
    float minScale { get; }
    float maxScale { get; }

    void SetScale();
}
