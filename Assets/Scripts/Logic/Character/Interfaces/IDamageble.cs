public interface IDamageble
{
    int currentHealth { get;}
    
    int minHealth{ get;}
   
    int maxHealth{ get;}

   void TakeDamage(int damageAmount);
}
