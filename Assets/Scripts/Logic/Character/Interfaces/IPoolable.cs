using UnityEngine;


public interface IPoolable
{
    string poolTag { get; }
    GameObject prefab { get; }
    void OnSpawn();
}
