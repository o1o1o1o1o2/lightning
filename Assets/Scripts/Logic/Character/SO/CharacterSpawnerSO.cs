using System;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterSpawner", menuName = "Character/CharacterSpawner", order = 1)]
public class CharacterSpawnerSO : ScriptableObjectManualDomainReload, ISpawnable
{
    [SerializeField]
    protected GameObject _prefab;
    
    protected CharController _charController;

    public CharController charController => _charController;

    [SerializeField] 
    protected IntReference _minHealth;
    
    [SerializeField] 
    protected IntReference _maxHealth;

    [SerializeField] 
    protected float _minScale;

    [SerializeField] 
    protected float _maxScale;

    [SerializeField] 
    protected FloatReference _spawnChance;
    public float spawnChance => _spawnChance;

    public virtual void Spawn()
    {
        IPoolable obj = SimplePool.Instance.GetFromPool(charController);
        obj.prefab.transform.position = RandomEXT.GetRandomPositionAtTheEdgeOfScreen(new Vector2(GameSettings.Instance.gridSize.x, GameSettings.Instance.gridSize.y));
        obj.OnSpawn();
    }
    
    protected override void OnEnable()
    {
        base.OnEnable();
        _charController = _prefab.GetComponent<CharController>();
        if (_charController == null)
        {
            throw new Exception($"Prefab {_prefab.name} doesn't have CharacterController attached");
        }

        charController.SetupCharacter(_minHealth, _maxHealth, _minScale, _maxScale, _prefab.name);
    }
}
