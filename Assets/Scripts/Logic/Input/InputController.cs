using UnityEngine;
using UnityEngine.Events;

public class InputController : Singleton<InputController>
{
    [SerializeField] private GameplayController gameplayController;

    private Vector2[] _touchesPositions;

    [SerializeField] private UnityEvent buttonBackEvent;

    private void Awake()
    {
        _touchesPositions = new Vector2[2];
    }
    
    private void OnEnable()
    {
        TouchInputManager.Instance.TouchProcessEvent += OnTouchProcess;
        TouchInputManager.Instance.TouchEndedEvent += OnTouchEnded;
        
        TouchInputManager.Instance.TwoFingersBeginEvent += OnTwoFingersTouchBegun;
        TouchInputManager.Instance.TwoFingersTouchProcessEvent += OnTwoFingersTouchProcessProcess;
        TouchInputManager.Instance.TwoFingersEndedEvent += OnTwoFingersTouchEnded;

        TouchInputManager.Instance.BackButtonEvent += OnBackButtonPrssed;
    }
    
    private void OnDisable()
    {
  
        TouchInputManager.Instance.TouchProcessEvent -= OnTouchProcess;
        TouchInputManager.Instance.TouchEndedEvent -= OnTouchEnded;
       
        TouchInputManager.Instance.TwoFingersBeginEvent -= OnTwoFingersTouchBegun;
        TouchInputManager.Instance.TwoFingersTouchProcessEvent -= OnTwoFingersTouchProcessProcess;
        TouchInputManager.Instance.TwoFingersEndedEvent -= OnTwoFingersTouchEnded;
        
        TouchInputManager.Instance.BackButtonEvent -= OnBackButtonPrssed;

    }

    private void OnTouchProcess(Touch touch)
    {
        gameplayController.SetTouchColliderPosition(touch.position);
    }

    private void OnTouchEnded(Touch touch)
    {
        gameplayController.SetTouchCollidersInactive();
    }

    private void OnTwoFingersTouchBegun(Touch[] touches)
    {
        gameplayController.SetLightningActive(true);
        OnTwoFingersTouchProcessProcess(touches);
    }
    
    private void OnTwoFingersTouchProcessProcess(Touch[] touches)
    {
        _touchesPositions[0] = touches[0].position;
        _touchesPositions[1] = touches[1].position;
        gameplayController.SetTouchColliderPositions(_touchesPositions);
    }
    
    private void OnTwoFingersTouchEnded(Touch[] touches)
    {
        gameplayController.SetLightningActive(false);
        gameplayController.SetTouchCollidersInactive();
    }

    private void OnBackButtonPrssed()
    {
        buttonBackEvent.Invoke();
    }
}
