using UnityEngine;
using UnityEngine.Events;

public class GameSettings : Singleton<GameSettings>
{
    
    [SerializeField] 
    public FloatReference attackSpeed;
    
    [SerializeField] 
    public FloatReference spawnRate;

    [SerializeField, SerializeProperty("camOrthographicSize")]
    private float _camOrthographicSize;
    public float camOrthographicSize
    {
        get => _camOrthographicSize;
        set {
            if (value >=0)
            {
                _gridSize.y = 2f * value;
                _gridSize.x = _gridSize.y * Camera.main.aspect;
                _camOrthographicSize = value;
                onGridSizeChanged.Invoke();
            }
        }
    }

    [SerializeField]
    private float _offset;
    public float Offset => _offset;

    [SerializeField]
    private Vector2 _gridSize;
    public Vector2 gridSize =>_gridSize;

    [SerializeField]
    private UnityEvent onGridSizeChanged;
}
