using TMPro;
using UnityEngine;

public class ScoresController : MonoBehaviour
{
    [SerializeField]
    private IntReference scores;

    private TextMeshProUGUI _scoresText;

    private void Awake()
    {
        _scoresText = GetComponent<TextMeshProUGUI>();
    }

    public void Update()
    { 
        _scoresText.SetText(scores.ToString());
    }
}
