using System.Collections.Generic;
using UnityEngine;

public class DamageCollider : MonoBehaviour
{
    private Dictionary<Collider,float> _colliderInfoList;

    private float _attackSpeed;

    public void SetAttackSpeed(float attackSpeed)
    {
        _attackSpeed = attackSpeed;
    }

    private void Awake()
    {
        _colliderInfoList = new Dictionary<Collider, float>();
        _attackSpeed = GameSettings.Instance.attackSpeed;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (!_colliderInfoList.ContainsKey(collider)) _colliderInfoList.Add(collider, _attackSpeed);
    }

    void OnTriggerExit(Collider collider)
    {
        _colliderInfoList.Remove(collider);
    }
    private void OnTriggerStay(Collider other)
    {
        if (_colliderInfoList.ContainsKey(other) && _colliderInfoList[other] >= _attackSpeed)
        {
            _colliderInfoList[other] = 0;

            other.gameObject.GetComponent<IDamageble>().TakeDamage(1);
        }
        
        _colliderInfoList[other] += Time.deltaTime;
    }
    
    
}
