using Fly.Utils;
using Logic;
using UnityEngine;
using UnityEngine.Events;

public class GameplayController : MonoBehaviour
{

    [SerializeField] private IntReference scores;

    [SerializeField] private float roundTime;
    
    [SerializeField] private Spawner spawner;
    [SerializeField] private Timer timer;
    
    [SerializeField] private BoxCollider lighningCollider;
    private Transform _lighningColliderTr;
    [SerializeField] 
    private ChainLightning chainLightning;
    [SerializeField]
    private CapsuleCollider[] touchCollider;
    private Camera _camera;
    private string _mapTag = "map";
    
    private Vector3 _angle;

    [SerializeField] private UnityEvent gameEnded;
    private bool _gameStoped;

    private DamageCollider _lighningDamageCollider; 
    private void Awake()
    {
        _camera = Camera.main;
        _lighningColliderTr = lighningCollider.transform;
        _lighningDamageCollider = lighningCollider.GetComponent<DamageCollider>();
    }

    public void SetTouchColliderPosition(Vector2 screenPosition)
    {
        if (_gameStoped) return;
        _camera.ScreenPointToHitWorldPositionWithTag(screenPosition,out Vector3 hit, _mapTag);
        hit.z = 0;
        touchCollider[0].transform.position = hit;
    }

    public void SetTouchColliderPositions(Vector2[] screenPositions)
    {
        if (_gameStoped) return;
        _camera.ScreenPointToHitWorldPositionWithTag(screenPositions[0],out Vector3 hit1, _mapTag);
        hit1.z = 0;
        touchCollider[0].transform.position = hit1;
        
        _camera.ScreenPointToHitWorldPositionWithTag(screenPositions[1],out Vector3 hit2, _mapTag);
        hit2.z = 0;
        touchCollider[1].transform.position = hit2;

        var dir = hit2 - hit1;
        _lighningColliderTr.position = (hit2 + hit1)*0.5f;
        
        var size = lighningCollider.size;
        size = new Vector3(dir.magnitude,size.y,size.z);
        lighningCollider.size = size;
        
        chainLightning.SetLineWidth(5/size.x);//todo size independent
        _lighningDamageCollider.SetAttackSpeed(size.x/8*GameSettings.Instance.attackSpeed);//todo size independent

        _angle = _lighningColliderTr.localRotation.eulerAngles;
        _angle.z = Vector3.right.Angle360(dir.normalized);
        _lighningColliderTr.localRotation = Quaternion.Euler(_angle);
        
        chainLightning.SetTouchPositions(hit1,hit2);
    }
    
    public void SetLightningActive(bool active)
    {
        if (_gameStoped) return;
        if (active) chainLightning.Activate(true);
        else
        {
            chainLightning.Activate(false);
            _lighningColliderTr.position = new Vector3(-10, 0, -10);
        }
    }

    public void SetTouchCollidersInactive()
    {
        touchCollider[0].transform.position = new Vector3(-10,0,-10);
        touchCollider[1].transform.position = new Vector3(-10,0,-10);
    }

    private void Update()
    {
        if (!_gameStoped && timer.currentTime > roundTime)
        {
            StopGame();
            _gameStoped = true;
            gameEnded.Invoke();
        };
    }

    public void StartGame()
    {
        scores.variable.Reinit();
        timer.StartTimer();
        _gameStoped = false;
        spawner.StartSpawn();
    }

    public void StopGame()
    {
        spawner.StopAllCoroutines();
        SetLightningActive(false);
        
        var chars = FindObjectsOfType<CharController>();
        foreach (var g in chars)
        {
            g.Deactivate();
        }
    }
    
    public void PauseGame()
    {
        _gameStoped = true;
        Time.timeScale = 0;
    }
    
    public void ResumeGame()
    {
        _gameStoped = false;
        Time.timeScale = 1;
    }
    
}
