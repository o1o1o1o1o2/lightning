using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

 namespace Logic
 {
     public class Spawner : MonoBehaviour
     {
         [SerializeField]
         private ScriptableObject[] itemsSO;
         
         private ISpawnable[] items;

         private Tuple<float,float>[] _itemChancesArr;
  
         private BinarySearchComparer _binarySearchComparer;

         private float _totalChance;
     
         void Start ()
         {
             if (itemsSO?.Length == 0) throw new Exception("No Items To Spawn");
             
             List<ISpawnable> tempList = new List<ISpawnable>();

             for (int i = 0; i < itemsSO.Length; i++) if (itemsSO[i] != null) tempList.Add((ISpawnable)itemsSO[i]);
             
             if (tempList.Count == 0) throw new Exception("No Items To Spawn");

             items = tempList.ToArray();

             _totalChance = 0;

             for (int i = 0; i < items.Length; i++) _totalChance += items[i].spawnChance;

             float beginningOfChance = 0;
             float endOfChance = 0;
             
             _itemChancesArr = new Tuple<float,float>[items.Length];
  
             for ( int i = 0; i < items.Length; i++) {
                 endOfChance = beginningOfChance + items[i].spawnChance / _totalChance;
                 _itemChancesArr[i] = new Tuple<float,float>(beginningOfChance, endOfChance);
                 beginningOfChance = endOfChance;
             }
  
             _binarySearchComparer = new BinarySearchComparer();
         }

         private void Spawn()
         {
             CurrentSpawn().Spawn();
         }
     
         private ISpawnable CurrentSpawn()
         {
             var r = Random.Range(0f, 1f);
             Tuple<float, float> chance = new Tuple<float, float>(r,r);

             int index = Array.BinarySearch(_itemChancesArr, chance, _binarySearchComparer);
             return items[index];
         }
     
         private class BinarySearchComparer : IComparer<Tuple<float, float>>
         {
             public int Compare( Tuple<float, float> chanceRange, Tuple<float, float> chance)
             {
                 if ( chanceRange?.Item2 <  chance?.Item1) return -1;
            
                 if (chanceRange?.Item1 > chance?.Item1) return 1;
  
                 return 0; 
             }
         }
         
         public void StartSpawn()
         {
             StartCoroutine(SpawnC());
         }

         IEnumerator SpawnC()
         {
             while (true)
             {
                 yield return new WaitForSeconds(GameSettings.Instance.spawnRate);
                 Spawn();
             }   
         }

  
     }
 }
