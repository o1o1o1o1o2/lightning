using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider))]
[ExecuteInEditMode]
public class LevelMap : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _mapSpriteRenderer;
    private BoxCollider _boxCollider;

    private void Awake()
    {
        _mapSpriteRenderer = GetComponent<SpriteRenderer>();
        _boxCollider = GetComponent<BoxCollider>();
        SetupGrids();
    }

    public void SetupGrids()
    {
        transform.position = new Vector3(GameSettings.Instance.gridSize.x / 2, GameSettings.Instance.gridSize.y / 2);
        _boxCollider.size = new Vector3(GameSettings.Instance.gridSize.x, GameSettings.Instance.gridSize.y, 0) + new Vector3(GameSettings.Instance.Offset, GameSettings.Instance.Offset, 0);
        _mapSpriteRenderer.size = GameSettings.Instance.gridSize + new Vector2(GameSettings.Instance.Offset, GameSettings.Instance.Offset);
    }
}
