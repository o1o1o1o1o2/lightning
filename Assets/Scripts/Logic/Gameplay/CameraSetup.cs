using System;
using UnityEngine;

[ExecuteInEditMode]
public class CameraSetup : MonoBehaviour
{

    private Camera _camera;
    private Transform _cameraTransform;

    public void SetCameraOrthographicSize()
    {
        _camera.orthographicSize = ((GameSettings.Instance.camOrthographicSize - GameSettings.Instance.Offset) >=0)?GameSettings.Instance.camOrthographicSize - GameSettings.Instance.Offset:0;
    }

    public void SetupCamera()
    {
        SetCameraOrthographicSize();
     
        if (_camera != null)
        {
            _cameraTransform.position = new Vector3( GameSettings.Instance.gridSize.x / 2, GameSettings.Instance.gridSize.y / 2, -10);
        }
    }

    public void Awake()
    {
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 1;

        _camera = Camera.main;
        if (_camera != null) _cameraTransform = _camera.transform;
        SetupCamera();
    }
    
}
