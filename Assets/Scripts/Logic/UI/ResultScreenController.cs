using TMPro;
using UnityEngine;

public class ResultScreenController : MonoBehaviour
{
    [SerializeField]
    private IntReference scores;

    [SerializeField]
    private TextMeshProUGUI _scoresText;

    public void OnEnable()
    { 
        _scoresText.SetText("Your total scores : " + scores);
    }
}
