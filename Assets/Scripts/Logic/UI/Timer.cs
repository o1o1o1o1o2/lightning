using System;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Timer : MonoBehaviour
{
    public TextMeshProUGUI min;
    public TextMeshProUGUI sec;
    public TextMeshProUGUI mil;

    public float currentTime{ get; private set; }

    private bool _active;

    public void StartTimer()
    {
        _active = true;
        currentTime = 0;
    }
    
    public void StopTimer()
    {
        currentTime = 0;
        _active = false;
    }

    public void PauseTimer()
    {
        _active = false;
    }
    
    public void ResumeTimer()
    {
        _active = true;
    }

    private string s1 = "{0:00}";
    private string s2 = "{0}";

    void Update()
    {
        if (_active)
        {
            currentTime += Time.deltaTime;
            min.SetText(s1, Mathf.Floor((currentTime) / 60));
            sec.SetText(s1, Mathf.Floor((currentTime) % 60));
            mil.SetText(s2, Mathf.Floor((currentTime) % 1 * 10));
        }
    }
}
