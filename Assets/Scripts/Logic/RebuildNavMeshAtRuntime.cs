using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RebuildNavMeshAtRuntime : MonoBehaviour
{
    [SerializeField] private NavMeshSurface nm;
    public void Rebuild()
    {
        nm.BuildNavMesh();
    }
   
    public void SetNavMeshSize()
    {
        nm.size = new Vector3(GameSettings.Instance.gridSize.x,0,GameSettings.Instance.gridSize.y) + new Vector3(GameSettings.Instance.Offset,0, GameSettings.Instance.Offset);
    }
}
