﻿using UnityEngine;

public class ChainLightning : MonoBehaviour
{
	[SerializeField] private GameObject lineRendererPrefab;

	private LineRenderer _lineRenderer;

	private Vector3 _startPos;
	private Vector3 _endPos;

	[SerializeField]
	private float refrashRate;

	private float _nextRefresh;
	[SerializeField] private float segmentLength = 0.3f;

	private LightningBolt lightningBolt { get; set; }

	private bool _isActive;

	void Awake()
	{
		_lineRenderer = Instantiate(lineRendererPrefab, transform).GetComponent<LineRenderer>();
		_isActive = false;
		lightningBolt = new LightningBolt(segmentLength,_lineRenderer);
	}

	public void Activate(bool active)
	{
		if (active)
		{
			_lineRenderer.enabled = true;
			_isActive = true;
		}
		else
		{
			_nextRefresh = 0;
			_lineRenderer.enabled = false;
			_isActive = false;
		}
	}

	public void SetTouchPositions(Vector3 startPos, Vector3 endPos)
	{
		_startPos = startPos;
		_endPos = endPos;
	}
	
	public void SetLineWidth(float width)
	{
		_lineRenderer.widthMultiplier = width;
	}

	void Update()
	{
		if (_isActive && Time.time > _nextRefresh)
		{
			lightningBolt.DrawLightning(_startPos, _endPos);
			_nextRefresh = Time.time + refrashRate;
		}
	}
}
