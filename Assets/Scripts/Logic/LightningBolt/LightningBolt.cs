﻿using UnityEngine;

public class LightningBolt
{
	private LineRenderer lineRenderer { get; set; }

	private float segmentLength { get; set; }
	

	public LightningBolt(float segmentLength, LineRenderer lineRenderer)
	{
		this.lineRenderer = lineRenderer;
		this.segmentLength = segmentLength;
	}

	public void DrawLightning(Vector2 startPos, Vector2 endPos)
	{
		float distance = Vector2.Distance(startPos, endPos);
		int segments = 5;
		if (distance > segmentLength)
		{
			segments = Mathf.FloorToInt(distance / segmentLength) + 2;
		}
		else
		{
			segments = 4;
		}
		
		lineRenderer.positionCount = segments;
		
		lineRenderer.SetPosition(0, startPos);
		
		Vector2 curPos;
		
		for (int j = 1; j < segments - 1; j++)
		{
			Vector2 tmp = Vector2.Lerp(startPos, endPos, j / (float) segments);
			
			curPos = new Vector2(tmp.x + Random.Range(-0.5f, 0.5f), tmp.y + Random.Range(-0.5f, 0.5f));
		
			lineRenderer.SetPosition(j, curPos);
		}

		lineRenderer.SetPosition(segments - 1, endPos);

	}
}
